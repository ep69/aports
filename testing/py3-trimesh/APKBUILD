# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=py3-trimesh
pkgver=3.18.1
pkgrel=0
pkgdesc="Python library for working with triangular meshes"
url="https://github.com/mikedh/trimesh"
# x86, armhf, armv7 Tests fail on int64 to int32 casts on these arches
# s390x, no py3-rtree
# riscv64, no py3-shapely
arch="noarch !x86 !armhf !armv7 !s390x !riscv64"
license="MIT"
depends="
	py3-colorlog
	py3-jsonschema
	py3-lxml
	py3-msgpack
	py3-networkx
	py3-pillow
	py3-requests
	py3-rtree
	py3-scipy
	py3-shapely
	py3-svgpath
	py3-mapbox-earcut
	python3
	"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-xdist py3-pyinstrument"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikedh/trimesh/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/trimesh-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# test_obj.py: no format zae, probably needs more investigation
	pytest -n $JOBS \
		--deselect tests/test_dae.py::DAETest::test_material_round \
		--deselect tests/test_dae.py::DAETest::test_obj_roundtrip \
		--deselect tests/test_light.py::LightTests::test_scene \
		--deselect tests/test_obj.py::OBJTest::test_multi_nodupe
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
72936c0fc6f89ffa35b80928dc5abac8ca4fac023f5be1d4adec6d020f3da6ee2c7a97efb0cecf58faad94ef6f33949533ac789affcecee07696ca0188fd47f0  py3-trimesh-3.18.1.tar.gz
"
