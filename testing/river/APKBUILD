# Contributor: Anjandev Momi <anjan@momi.ca>
# Maintainer: Anjandev Momi <anjan@momi.ca>
pkgname=river
pkgver=0.2.2
pkgrel=0
pkgdesc="Dynamic Tiling Wayland Compositor"
url="https://github.com/riverwm/river"
arch="x86_64 aarch64" # limited by zig aport
license="GPL-3.0-only"
makedepends="
	libevdev-dev
	libxkbcommon-dev
	pixman-dev
	scdoc
	wayland-dev
	wayland-protocols
	wlroots-dev
	zig
	"
depends="seatd"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/riverwm/river/releases/download/v$pkgver/river-$pkgver.tar.gz
	zig-wayland-aarch64.patch
	"

# We may want other than "baseline" for other targets, when enabled by zig
case "$CARCH" in
	aarch64|x86_64) cputarget=baseline ;;
esac

build() {
	# This installs it to $builddir/out
	DESTDIR="$builddir/out" zig build -Drelease-safe -Dxwayland --prefix /usr install \
		${cputarget:+-Dcpu="$cputarget"}
}

check() {
	zig build test
}

package() {
	mkdir -p "$pkgdir"
	cp -r out/* "$pkgdir"

	# Fix location of pkgconfig files, must be fixed upstream
	mkdir -p "$pkgdir"/usr/lib
	mv "$pkgdir"/usr/share/pkgconfig "$pkgdir"/usr/lib

	# Install example configuration
	install -Dm0644 example/init -t "$pkgdir"/usr/share/doc/river/examples

	# Fix location of fish completion
	mkdir -p "$pkgdir"/usr/share/fish/completions/
	mv "$pkgdir"/usr/share/fish/vendor_completions.d/*.fish \
		"$pkgdir"/usr/share/fish/completions/
	rm -rf "$pkgdir"/usr/share/fish/vendor_completions.d
}

sha512sums="
1c579f74321632271082c92b743aac751863374a07f0ff1b461d93475796b991b1af5a77913e353d87a1d983ac0aec1bf0d610983180194f3e1e0c7b16ae6231  river-0.2.2.tar.gz
6cd898effefe4656658d0a914b9322ea73b2db314764af969746f1e34efe409bd7f4412eb6bc165eb723542375fc8dba7655db3deb56ba6f1236a9c99959c8b9  zig-wayland-aarch64.patch
"
