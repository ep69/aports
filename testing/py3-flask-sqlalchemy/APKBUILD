# Contributor:
# Maintainer: Philipp Glaum <p@pglaum.de>
pkgname=py3-flask-sqlalchemy
_pkgname=Flask-SQLAlchemy
pkgver=3.0.2
pkgrel=0
pkgdesc="Adds SQLAlchemy support to your Flask application."
url="https://pypi.org/project/Flask-SQLAlchemy/"
arch="noarch"
license="BSD-3-Clause"
depends="python3 py3-flask py3-greenlet py3-sqlalchemy"
makedepends="py3-gpep517 py3-installer py3-pdm-backend py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-mock"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"
options="!check" # fail with new everything, removed in latest version

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	PYTHONPATH=build/lib \
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
89fa71796efe53e993ef8fc264b04f64c3174cadc960a9e51944e4fe70513a1c91e244e55f70690623f1d7d6d7c215729581a6989484bb0369b4bb0e412f499c  Flask-SQLAlchemy-3.0.2.tar.gz
"
