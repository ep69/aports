# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=hyperlink
pkgver=0.1.26
pkgrel=0
pkgdesc="Very fast link checker for CI"
url="https://github.com/untitaker/hyperlink"
# riscv64: fails to build libc crate
# armhf: builds freeze and time out
arch="all !riscv64 !armhf"
license="MIT"
makedepends="cargo"
source="$pkgname-$pkgver.tar.gz::https://github.com/untitaker/hyperlink/archive/refs/tags/$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --release --frozen
}

check() {
	# Tests try to execute the compiled binary. Without `--release`, they
	# attempt to run the debug binary, which is not present and tests fail.
	cargo test --release --frozen
}

package() {
	install -Dm 0755 "target/release/$pkgname" "$pkgdir/usr/bin/$pkgname"
}

sha512sums="
8f558db4c4d96e0cb1641e985c791d091abf432d0387975663fcb3aae94787f1e4a858c80fc275a7300c90cf633e0a33be266b0d302f218114dc5266a897848f  hyperlink-0.1.26.tar.gz
"
