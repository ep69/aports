# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdesu
pkgver=5.102.0
pkgrel=0
pkgdesc="Integration with su for elevated privileges"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kpty-dev
	kservice-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdesu-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
# Since the goal of this library is to elevate privileges, suid being needed should be obvious
options="suid"

[ "$CARCH" = "ppc64le" ] && options="$options !check" # kdesutest is broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d549e4b14b3e4cc5f78fd73b824a7ca0fc8f3d41c71afc6f7f6093ce5962a83bb2d99d6eeee033afd668a1bd01039f7142b77a0cb3973226574805158efc9bab  kdesu-5.102.0.tar.xz
"
