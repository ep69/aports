# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdnssd
pkgver=5.102.0
pkgrel=0
arch="all !armhf" # armhf blocked by extra-cmake-modules
pkgdesc="Network service discovery using Zeroconf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends="avahi"
depends_dev="
	avahi-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdnssd-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e1e2b12e2c55bf316966d1ffb02b1c7edf84f68e17bddfd5d73bc2bc97565bbf7597481dca37dd30f31f98f0d17d6d3940fbc3c77cee3376af9daa08c43ed943  kdnssd-5.102.0.tar.xz
"
