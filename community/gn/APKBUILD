# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=gn
pkgver=0_git20221212
pkgrel=1
_commit=5e19d2fb166fbd4f6f32147fbb2f497091a54ad8
pkgdesc="Meta-build system that generates build files for Ninja"
arch="all"
url="https://gn.googlesource.com/gn"
license="BSD-3-Clause"
depends="samurai"
makedepends="python3"
# archive needs to include .git for the build script to be able to determine the version
source="https://dev.alpinelinux.org/archive/gn/gn-$_commit.tar.xz"
builddir="$srcdir/gn"

[ "$CARCH" = "riscv64" ] && options="$options textrels"

_disturl="dev.alpinelinux.org:/archive/gn/"
snapshot() {
	clean
	deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone https://gn.googlesource.com/gn
	tar c gn | xz -T0 -9 -e -vv - > "$SRCDEST"/gn-$_commit.tar.xz
	rsync --progress -La "$SRCDEST"/gn-$_commit.tar.xz psykose@$_disturl
}

build() {
	python3 ./build/gen.py \
		--no-static-libstdc++ \
		--no-strip
	ninja -C out
}

check() {
	./out/gn_unittests
}

package() {
	install -Dm755 out/gn "$pkgdir"/usr/bin/gn
}

sha512sums="
bb022ab957af3ece4f510ade77041ae501b82674580a5602ffc21eb0598f2d3817643e4420b1eb518ee153c3cc926df241f1c3ab472a8cef563c08986b6ac76e  gn-5e19d2fb166fbd4f6f32147fbb2f497091a54ad8.tar.xz
"
