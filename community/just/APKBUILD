# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=just
pkgver=1.11.0
pkgrel=0
pkgdesc="Just a command runner"
url="https://github.com/casey/just"
# riscv64: rust currently broken on this arch
# s390x: blocked by nix crate
arch="all !riscv64 !s390x"
license="CC0-1.0"
checkdepends="bash fzf"
makedepends="cargo"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/casey/just/archive/$pkgver/just-$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_OPT_LEVEL="z"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --release --frozen
}

check() {
	# Skipped tests are somehow broken.
	cargo test --frozen -- \
		--skip choose::default \
		--skip edit::editor_precedence \
		--skip choose::multiple_recipes \
		--skip functions::env_var_functions
}

package() {
	cargo install --locked --offline --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*

	install -D -m 644 completions/just.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -D -m 644 completions/just.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -D -m 644 completions/just.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
eccf57e4d10d339d57c5082117563a0ba15f2ad2818b45c2e5e953ffe6ff6742957d4ec33e80170c92efb82277cbdf02327aa319f4e2a3cd3d8b58463178027e  just-1.11.0.tar.gz
"
