# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-desktop
pkgver=43.1
pkgrel=0
pkgdesc="GNOME desktop core libraries"
url="https://gitlab.gnome.org/GNOME/gnome-desktop"
arch="all"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends_dev="gobject-introspection-dev gsettings-desktop-schemas-dev"
makedepends="$depends_dev
	eudev-dev
	fontconfig-dev
	gdk-pixbuf-dev
	glib-dev
	gtk-doc
	gtk+3.0-dev
	gtk4.0-dev
	iso-codes-dev
	itstool
	libseccomp-dev
	libxml2-utils
	meson
	xkeyboard-config-dev
	"
depends="bubblewrap musl-locales"
options="!check" # Can't find its own GSettings schemas
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-dbg"
source="https://download.gnome.org/sources/gnome-desktop/${pkgver%.*}/gnome-desktop-$pkgver.tar.xz"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dudev=enabled \
		-Dbuild_gtk4=true \
		-Dlegacy_library=true \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}
sha512sums="
6b6c9597629ec32453ba1823a3508616a951c0274400e4814cd5b39438ee2c752842cbd37a8dc4f19e9a667a7f171546895ebb15a2a79802171466e395e9b3a4  gnome-desktop-43.1.tar.xz
"
