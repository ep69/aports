# Contributor: k0r10n <k0r10n.dev@gmail.com>
# Contributor: Ivan Tham <pickfire@riseup.net>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=extra-cmake-modules
pkgver=5.102.0
pkgrel=0
pkgdesc="Extra CMake modules"
url="https://invent.kde.org/frameworks/extra-cmake-modules"
arch="noarch"
license="BSD-3-Clause"
depends="cmake"
makedepends="py3-sphinx samurai"
checkdepends="qt5-qtbase-dev qt5-qtquickcontrols2-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/extra-cmake-modules-$pkgver.tar.xz"
subpackages="$pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DSphinx_BUILD_EXECUTABLE=/usr/bin/sphinx-build
	cmake --build build
}

check() {
	cd build
	# Broken tests
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(KDEFetchTranslations|KDEInstallDirsTest.relative_or_absolute_usr|KDEInstallDirsTest.relative_or_absolute_qt)"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
aa947bf03d296d193b2407b3f381414116c48e2ad7e8a7e499d00868aa85cde483599e9ce28f87f5dcf2973df84c6322e75bfc7a277af9e032706e32420553ce  extra-cmake-modules-5.102.0.tar.xz
"
