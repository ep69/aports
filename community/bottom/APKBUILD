# Contributor: guddaff <guddaff@protonmail.com>
# Maintainer: guddaff <guddaff@protonmail.com>
pkgname=bottom
pkgver=0.7.1
pkgrel=0
pkgdesc="Graphical process/system monitor with a customizable interface"
url="https://github.com/ClementTsang/bottom"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
license="MIT"
makedepends="cargo"
subpackages="
	$pkgname-fish-completion
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-doc
	"
source="https://github.com/ClementTsang/bottom/archive/$pkgver/bottom-$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	BTM_GENERATE=true cargo build --frozen --release
}

check() {
	CARGO_HUSKY_DONT_INSTALL_HOOKS=true cargo test --frozen
}

package() {
	cargo install --locked --path . --root=$pkgdir/usr
	rm "$pkgdir"/usr/.crates*

	install -Dm644 sample_configs/default_config.toml -t "$pkgdir"/usr/share/doc/$pkgname/

	cd target/tmp/bottom/completion
	install -Dm644 _btm "$pkgdir"/usr/share/zsh/site-functions/_btm
	install -Dm644 btm.bash "$pkgdir"/usr/share/bash-completion/completions/btm
	install -Dm644 btm.fish "$pkgdir"/usr/share/fish/completions//btm.fish
}

sha512sums="
6f19f4eb79cbec2e0d35dd05b23abbb93da772c1e9e3f9c11b3a981e2077759665ff52e2f294b136324df18aa67942474c11f24bbb694822cfbb91b5a6d22270  bottom-0.7.1.tar.gz
"
