# Contributor: Henrik Riomar <henrik.riomar@gmail.com>
# Contributor: Drew DeVault <sir@cmpwn.com>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=wlroots
pkgver=0.16.1
pkgrel=1
pkgdesc="Modular Wayland compositor library"
url="https://gitlab.freedesktop.org/wlroots/wlroots"
license="MIT"
arch="all"
makedepends="
	eudev-dev
	glslang-dev
	hwdata-dev
	libcap-dev
	libinput-dev
	libseat-dev
	libxcb-dev
	libxkbcommon-dev
	mesa-dev
	meson
	ninja
	pixman-dev
	vulkan-loader-dev
	wayland-dev
	wayland-protocols
	xcb-util-image-dev
	xcb-util-renderutil-dev
	xcb-util-wm-dev
	xkeyboard-config-dev
	xwayland-dev
	"
subpackages="$pkgname-dbg $pkgname-dev"
source="https://gitlab.freedesktop.org/wlroots/wlroots/-/archive/$pkgver/wlroots-$pkgver.tar.gz
	$pkgname-segfault-fix.patch::https://gitlab.freedesktop.org/wlroots/wlroots/-/commit/27a0b8b74f5662551ae1e17c9ac27fb1595cdbbd.patch
	"
options="!check" # no test suite

build() {
	abuild-meson \
		-Db_lto=true \
		-Dexamples=false \
		. build
	meson compile -C build
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C build
}

sha512sums="
a590960d3a228f1fec007d20094e48d9276568f59e2ce3bac56889fb7df519d68ffb81a73130b7bbf350ec95056009aaabb18ada52a97486d6ad1d5deb6844c3  wlroots-0.16.1.tar.gz
75e66e3c8aae2d47a75d2586d0a7fa6112f1456dfeb2468b1d30bf54713c222ed0fce0988674af87b6c439ba50bd7ca8ab20a49ca54b2424ce92fc64af5b2d01  wlroots-segfault-fix.patch
"
