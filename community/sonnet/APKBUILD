# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=sonnet
pkgver=5.102.0
pkgrel=0
pkgdesc="Spelling framework for Qt5"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends="hunspell"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	hunspell-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/sonnet-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# sonnet-test_autodetect fails to detect a speller backend
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "sonnet-test_autodetect"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
85bd83954247b5d676b63cf7333e18d936b471138da7509a642e06074ba936dbb0b07723bdacc8d1508b05ffe84748585e98a34bae76836a6378e4c42a58800e  sonnet-5.102.0.tar.xz
"
