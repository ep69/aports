# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=modemmanager-qt
pkgver=5.102.0
pkgrel=0
pkgdesc="Qt wrapper for ModemManager DBus API"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	modemmanager-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz-dev
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # requires dbus running
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/modemmanager-qt-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1db89d5baba077d9f5795b281002b306817f1b5d30198b3c05807dc82e2dd60ac7de2c34a023f04ceb84c1643fa6f1bd79c0f455c29e71b93b5a064e40e7644a  modemmanager-qt-5.102.0.tar.xz
"
