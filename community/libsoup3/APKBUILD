# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=libsoup3
pkgver=3.3.0
pkgrel=0
pkgdesc="Gnome HTTP client/server Library"
url="https://wiki.gnome.org/Projects/libsoup"
arch="all"
license="LGPL-2.0-or-later"
subpackages="$pkgname-dev $pkgname-lang $pkgname-dbg"
depends="glib-networking gsettings-desktop-schemas"
makedepends="
	brotli>=1.0.9-r1
	gobject-introspection-dev
	libgcrypt-dev
	libgpg-error-dev
	libpsl-dev
	libxml2-dev
	meson
	nghttp2-dev
	sqlite-dev
	vala
	zlib-dev
	"
checkdepends="gnutls-dev"
source="https://download.gnome.org/sources/libsoup/${pkgver%.*}/libsoup-$pkgver.tar.xz"
builddir="$srcdir/libsoup-$pkgver"

case "$CARCH" in
x86*)
	;;
*)
	# arm*: sigill for some reason
	# rest: sigabrt, http1 != http2 on localhost req
	options="$options !check"
	;;
esac

build() {
	abuild-meson \
		-Db_lto=true \
		-Dtls_check=false \
		-Dintrospection=enabled \
		-Dvapi=enabled \
		-Dtests="$(want_check && echo true || echo false)" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test -t 10 --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
583a6b57c2538e4d62df121e84a4940f2405f4fcdcdf85401929cd0a64db8cb3be84d9c979d1ce43827b474c647e36f0ef3ac764076cbe1b0b57865615c8817b  libsoup-3.3.0.tar.xz
"
