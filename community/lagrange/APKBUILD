# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=lagrange
pkgver=1.14.2
pkgrel=0
pkgdesc="Beautiful Gemini client"
url="https://gmi.skyjake.fi/lagrange"
license="BSD-2-Clause"
arch="all"
makedepends="
	cmake
	fribidi-dev
	harfbuzz-dev
	libunistring-dev
	libwebp-dev
	mpg123-dev
	openssl-dev
	pcre2-dev
	samurai
	sdl2-dev
	zip
	zlib-dev
	"
subpackages="$pkgname-doc"
source="https://git.skyjake.fi/gemini/lagrange/releases/download/v$pkgver/lagrange-$pkgver.tar.gz"
options="!check" # no test suite

[ "$CARCH" = "riscv64" ] && options="$options textrels"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_POPUP_MENUS=OFF \
		-DENABLE_RESIZE_DRAW=OFF
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1a504192916a803f8a53bf4da73d64842939db4850361b60f4b54804c9c65ea078b054820af759d5b6fc4d16509a95e679bce2c2912444762116c0d0f6bfb697  lagrange-1.14.2.tar.gz
"
