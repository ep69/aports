# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=networkmanager-qt
pkgver=5.102.0
pkgrel=0
pkgdesc="Qt wrapper for NetworkManager API"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="networkmanager"
depends_dev="networkmanager-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/networkmanager-qt-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# The excluded tests currently fail
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(manager|settings|activeconnection)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
46f9bebd65c2166dd2f736377d38d77ae3e26dfae2ce99c6198a6128d3fd4a5e0488e1d78c3c8d6013c81d09c54326a880fa1da54bed010b2dc01c202959cb67  networkmanager-qt-5.102.0.tar.xz
"
