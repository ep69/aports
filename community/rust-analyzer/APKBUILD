# Contributor: S.M Mukarram Nainar <theone@sm2n.ca>
# Maintainer: S.M Mukarram Nainar <theone@sm2n.ca>
pkgname=rust-analyzer
pkgver=2023.01.16
_pkgver=${pkgver//./-}
pkgrel=0
pkgdesc="A Rust compiler front-end for IDEs"
url="https://github.com/rust-lang/rust-analyzer"
# armhf, armv7, x86: some tests fail, not supported by upstream
# riscv64, s390x: blocked by cargo/rust
arch="aarch64 ppc64le x86_64"
license="MIT OR Apache-2.0"
depends="rust-src"
makedepends="cargo"
checkdepends="rustfmt"
subpackages="$pkgname-doc"
source="https://github.com/rust-lang/rust-analyzer/archive/$_pkgver/rust-analyzer-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-$_pkgver"
# requires rustup toolchain to run rustup rustfmt for sourcegen
options="net !check"

# crashes otherwise
unset CARGO_PROFILE_RELEASE_PANIC

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	CFG_RELEASE="$pkgver" cargo build --frozen --release --manifest-path crates/rust-analyzer/Cargo.toml
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rust-analyzer -t "$pkgdir"/usr/bin/
	install -Dm644 docs/user/manual.adoc -t "$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="
d68305b53c47d9b189396353172131fe539a7f4d1f163eb3c683c13f4dcd83cba84ebb85ac9abaf526c927a9877a9719adf854b6dcbef3a298984abb4415515d  rust-analyzer-2023.01.16.tar.gz
"
