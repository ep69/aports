# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-poetry-core
_pkgname=poetry-core
pkgver=1.4.0
pkgrel=0
pkgdesc="PEP 517 build backend implementation for Poetry"
url="https://github.com/python-poetry/poetry-core"
arch="noarch"
license="MIT"
depends="python3 py3-jsonschema py3-lark-parser py3-packaging py3-tomlkit"
makedepends="py3-gpep517 py3-installer"
checkdepends="py3-pytest py3-pytest-mock py3-virtualenv python3-dev py3-setuptools py3-pip py3-build"
source="$_pkgname-$pkgver.tar.gz::https://github.com/python-poetry/poetry-core/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest \
		--deselect tests/masonry/builders/test_sdist.py::test_includes_with_inline_table \
		--deselect tests/masonry/builders/test_sdist.py::test_default_with_excluded_data \
		--deselect tests/masonry/builders/test_wheel.py::test_default_src_with_excluded_data
	testenv/bin/python3 -m pytest --integration tests/integration
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
	# remove vendored versions of installed modules
	local sitedir=$(python3 -c "import site; print(site.getsitepackages()[0])")
	rm -r "$pkgdir/$sitedir"/poetry/core/_vendor
}

sha512sums="
31c9b42ae8e4ec5c4cec93604a5ea72642fb89717867d7f43fdf930835c7e1da97b0251870d86841c30b1bf0ee7b167fc6a8dcfe939beb5b40acd7e24dfaede7  poetry-core-1.4.0.tar.gz
"
