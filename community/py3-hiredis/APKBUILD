# Maintainer: Eivind Uggedal <eu@eju.no>
pkgname=py3-hiredis
pkgver=2.1.1
pkgrel=1
pkgdesc="Python extension that wraps hiredis"
url="https://github.com/redis/hiredis-py"
arch="all"
license="BSD-3-Clause"
makedepends="python3-dev py3-gpep517 py3-wheel py3-setuptools"
checkdepends="py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/h/hiredis/hiredis-$pkgver.tar.gz"
builddir="$srcdir"/hiredis-$pkgver
options="!check" # no tests in tarball

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
ae49f898948299acdeeb45df6e5b8e9c5bcec7e5255b6ca9877024ddb4342c423ecc16430e20cd0b1cf91dbb87481425684d9b5869caf36544bf8ae9af2aabdc  py3-hiredis-2.1.1.tar.gz
"
