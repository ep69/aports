# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=bluez-qt
pkgver=5.102.0
pkgrel=0
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
pkgdesc="Qt wrapper for Bluez 5 DBus API"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev"
makedepends="$depends_dev extra-cmake-modules doxygen graphviz qt5-qttools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/bluez-qt-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Multiple tests either hang or fail completely

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
07d6154f4ccc335372362564fe7cffa8e6ab5657e90e220c10c9b79ed2006058e63c49a9bae3a00de18d156bba9c1cb851a45fbda3f2ee52d10a2440fe6f39b4  bluez-qt-5.102.0.tar.xz
"
