# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kimageformats
pkgver=5.102.0
pkgrel=0
pkgdesc="Image format plugins for Qt5"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	libavif-dev
	libheif-dev
	libjxl-dev
	libraw-dev
	openexr-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kimageformats-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKIMAGEFORMATS_HEIF=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kimageformats-read-psd"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2582631b36e22ec6440cff6e6ba052cfca158331317e13f47c781486a824b2f6a8b44c6b715ced35e9087b0a649a87627ac3e72e0be0694d4e25fcaebcbacbb4  kimageformats-5.102.0.tar.xz
"
